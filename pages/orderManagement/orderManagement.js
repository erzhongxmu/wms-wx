var app = getApp();
const pathUrl = app.globalData.pathUrl;
Page({
  data: {
    index: 0,
    pathUrl: pathUrl,
    openId: '',
    name:'',
    list: [],
    bpmStatus:'',
    pageNumber: 0,
    loading: false, //"上拉加载"的变量，默认false，隐藏  
    loadingComplete: false,  //“没有数据”的变量，默认false，隐藏
  },
  onLoad: function (e) {
    var openId = wx.getStorageSync('openId');
    console.log(e)
      var proName = e.proName;
      var bpmStatus = e.bpmStatus
    this.setData({
        openId: openId,
        proName : proName,
        bpmStatus: bpmStatus
    });
      wx.setNavigationBarTitle({
          title: proName
      })
    this.fxjProMainController();
  },
    goApplyMortgage: function (e) {
        var proNo = e.currentTarget.dataset.prono;
        var proName = e.currentTarget.dataset.proname;
        wx.navigateTo({
            url: "/pages/applyMortgage/applyMortgage?proNo=" + proNo + '&proName=' + proName
        })
    },
  //产品详情
  fxjProMainController: function () {
    var that = this;
    wx.request({
        url: pathUrl + '/rest/wmom/list/' + that.data.openId,
      data: {
        bpmStatus:that.data.bpmStatus,
        pageNumber: 1,
        pageSize: 10
      },
      success: function (res) {
        if (res.data.ok || res.data.data) {
            if (res.data.data.length > 0){
                that.setData({
                    list: res.data.data
                })
            }else{
                that.setData({
                    loadingComplete: true, //把“没有数据”设为true，显示  
                    loading: false //把"上拉加载"的变量设为false，隐藏  
                })
            }
            
        } else {
          wx.showModal({
            title: '提示',
            content: '获取数据失败',
            confirmColor: '#7aa6d1',
            showCancel: false
          })
        }
      }
    })
  },
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.fxjProMainController();
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})