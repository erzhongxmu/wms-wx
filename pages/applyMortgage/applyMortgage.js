var app = getApp();
const pathUrl = app.globalData.pathUrl;
Page({
    data: {
        pathUrl: pathUrl,
        openId: '',
        proNo: '',
        detailInfo:'',
        phoneNumber:'',
        proName:'',
        posWeidu:24,
        posJingdu:118
    },
    onLoad: function(e) {
        var openId = wx.getStorageSync('openId');
        var proNo = e.proNo;
        var proName = e.proName;
        var id = e.id;
        this.setData({
            proNo: proNo,
            openId: openId,
            proName: proName
        })
        wx.setNavigationBarTitle({
            title: proName
        })
        console.log("proNo:" + proNo);
        this.detailInfo();
    },
    
    //获取详情
    detailInfo: function() {
        var that = this;
        wx.request({
            url: pathUrl + '/rest/posDetailController/' + that.data.proNo,
            success: function(res) {
                if (res.data.ok || res.data.data) {
                    that.setData({
                      detailInfo: res.data.data,
                      phoneNumber: res.data.data.posMobile,
                        posJingdu: res.data.data.posJingdu,
                        posWeidu: res.data.data.posWeidu
                    })
                } else {
                    wx.showModal({
                        title: '提示',
                        content: '获取数据失败',
                        confirmColor: '#7aa6d1',
                        showCancel: false
                    })
                }
            }
        })
    },
    // 评估
    goPetitionPhotos: function() {
      var that = this;
      wx.navigateTo({
        url: "/pages/petitionPhotos/petitionPhotos?orderNo=" + that.data.proNo + '&proName=' + that.data.proName
      })
    },
    // 拨打电话
    callPhone:function() {
      var that = this;
      wx.makePhoneCall({
        phoneNumber: that.data.phoneNumber 
      })
    },
    // 获取经纬度
    gotGps:function(){
      wx.getLocation({
        type: 'wgs84',
        success(res) {
          const latitude = res.latitude
          const longitude = res.longitude
          const speed = res.speed
          const accuracy = res.accuracy
          console.log('latitude==' + res.latitude + '=longitude=' + res.longitude)
        }
      });
    },
    // // 导航
    mapNavigation: function() {
        var that = this;
        console.log('lati==' + that.data.posWeidu + '=long=' + that.data.posJingdu);
        console.log(typeof (that.data.posWeidu))
        wx.openLocation({
            latitude: that.data.posWeidu,
            longitude: that.data.posJingdu,
            scale: 18
        })
    },
    openGps: function () {
        var that = this;
        wx.chooseLocation({
            success: function (res) {
                // that.setData({
                //     latitude: res.latitude,
                //     longitude: res.longitude
                // })
            }
        })
    },
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})