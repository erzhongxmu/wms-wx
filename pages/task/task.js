var app = getApp();
const Utils = require('../../utils/util.js')

const pathUrl = app.globalData.pathUrl;
Page({
    data: {
        pageHidden: true,
        pathUrl: pathUrl,
        taskIndex:0,
        bpmStatus: '1' ,
        tabBars: [
            { name: '待送定单', bpmStatus:'1' },
            { name: '已完成定单', bpmStatus: '已完成'}
        ],
        list:[],
        pageNumber: 0,
        loading: false, //"上拉加载"的变量，默认false，隐藏  
        loadingComplete: false,  //“没有数据”的变量，默认false，隐藏
    },
    onLoad: function (options) {
      var openId = wx.getStorageSync('openId');
      this.setData({
        openId: openId
      })
    }, 
    clickTab:function(e){
        var that = this;
        if (this.data.taskIndex === e.currentTarget.dataset.index) {
            return false;
        } else {
            wx.showLoading({
                title: '加载中',
            });
            that.setData({
                taskIndex: e.currentTarget.dataset.index,
                bpmStatus: e.currentTarget.dataset.bpmstatus,
                loadingComplete: false, //把“没有数据”设为true，显示  
                loading: false,//把"上拉加载"的变量设为false，隐藏  
                pageNumber: 0,
                list: []
            });
            if (e.currentTarget.dataset.index == 0){
                // console.log('待安装');
                this.onReachBottomTwo()
            }
            if (e.currentTarget.dataset.index == 1){
                // console.log('待检修')
                this.onReachBottom()
            }
            // this.onReachBottom();
        }
    },
    goApplyMortgage:function(e) {
        var proNo = e.currentTarget.dataset.prono;
        var proName = e.currentTarget.dataset.proname;
        wx.navigateTo({
            url: "/pages/applyMortgage/applyMortgage?proNo=" + proNo + '&proName=' + proName
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        wx.showLoading({
            title: '加载中',
        })
        var that =  this;
        that.setData({
            pageHidden: true,
            taskIndex: 0,
            loadingComplete: false, //把“没有数据”设为true，显示  
            loading: false,//把"上拉加载"的变量设为false，隐藏  
            pageNumber: 0,
            list: []
        })
        // this.onReachBottom();
        if (that.data.taskIndex==0){
            this.onReachBottomTwo();
        }
        if (that.data.taskIndex == 1) {
            this.onReachBottom();
        }
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        wx.showLoading({
            title: '加载中',
        })
        var that = this;
        that.setData({
            loadingComplete: false, //把“没有数据”设为true，显示  
            loading: false,//把"上拉加载"的变量设为false，隐藏  
            pageNumber: 0,
            taskIndex: taskIndex,
            list: []
        });
        // this.onReachBottom();
        if (that.data.taskIndex == 0) {
            this.onReachBottomTwo();
        }
        if (that.data.taskIndex == 1) {
            this.onReachBottom();
        }
        wx.stopPullDownRefresh();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    // 待检修
    onReachBottom: function () {
        var that = this;
        var pageNumber = that.data.pageNumber + 1;
        var allList = that.data.list;
        wx.request({
            url: pathUrl + '/rest/wmom/list/' + that.data.openId,
            data: {
                bpmStatus: '1',
                posXjType:'检修',
                pageNumber: pageNumber,
                pageSize: 10
            },
            success: function (res) {
                console.log(res.data)
                if (res.data.ok) {
                    wx.hideLoading();
                    that.setData({
                        pageHidden: false
                    })
                    if(res.data.data.length > 0){
                        allList = allList.concat(res.data.data);
                        if (res.data.data.length < 10) {
                            that.setData({
                                pageNumber: pageNumber,
                                list: allList,
                                loadingComplete: true, //把“没有数据”设为true，显示  
                                loading: false //把"上拉加载"的变量设为false，隐藏  
                            })
                        } else {
                            that.setData({
                                pageNumber: pageNumber,
                                list: allList,
                                loading: true
                            })
                        }
                    }else {
                        that.setData({
                            loadingComplete: true, //把“没有数据”设为true，显示  
                            loading: false //把"上拉加载"的变量设为false，隐藏  
                        })
                    }
                } else {
                    wx.hideLoading();
                    that.setData({
                        pageHidden: false
                    })
                    wx.showModal({
                        title: '提示',
                        content: '获取数据失败',
                        confirmColor: '#7aa6d1',
                        showCancel: false
                    })
                }
            }
        })
    },
    // 待安装
    onReachBottomTwo: function () {
        var that = this;
        var pageNumber = that.data.pageNumber + 1;
        var allList = that.data.list;
        wx.request({
            url: pathUrl + '/rest/wmom/list/' + that.data.openId,
            data: {
                bpmStatus: '1',
                pageNumber: pageNumber,
                pageSize: 10
            },
            success: function (res) {
                console.log(res.data)
                if (res.data.ok) {
                    wx.hideLoading();
                    that.setData({
                        pageHidden: false
                    })
                    if (res.data.data.length > 0) {
                        allList = allList.concat(res.data.data);
                        if (res.data.data.length < 10) {
                            that.setData({
                                pageNumber: pageNumber,
                                list: allList,
                                loadingComplete: true, //把“没有数据”设为true，显示  
                                loading: false //把"上拉加载"的变量设为false，隐藏  
                            })
                        } else {
                            that.setData({
                                pageNumber: pageNumber,
                                list: allList,
                                loading: true
                            })
                        }
                    } else {
                        that.setData({
                            loadingComplete: true, //把“没有数据”设为true，显示  
                            loading: false //把"上拉加载"的变量设为false，隐藏  
                        })
                    }
                } else {
                    wx.hideLoading();
                    that.setData({
                        pageHidden: false
                    })
                    wx.showModal({
                        title: '提示',
                        content: '获取数据失败',
                        confirmColor: '#7aa6d1',
                        showCancel: false
                    })
                }
            }
        })
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})