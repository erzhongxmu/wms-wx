var app = getApp();
const pathUrl = app.globalData.pathUrl;
Page({
    data: {
        pathUrl: pathUrl,
        orderNo: '',
        id: '',
        proName: '',
        latitude: '', //纬度
        longitude: '', //经度
        posRemark: "",
        msgcontent:"",
        bpmStatus:""
    },
    onLoad: function (e) {
        console.log(e)
        this.setData({
            id: e.orderNo,
            orderNo: e.orderNo,
            proName: e.proName
        });
        this.gotGps();
        this.getPosRemark();
    },
    msgcontentInput:function(e){
        var val = e.detail.value;
        this.setData({
            posRemark: val
        });

    },
    // 获取信息
    getPosRemark:function(){
        var that = this;
        wx.request({
            url: pathUrl + '/rest/posDetailController/' + that.data.id,
            method: 'GET',
            success: function (res){
                if(res.data.ok){
                    that.setData({
                        msgcontent: res.data.data.posRemark,
                        bpmStatus:res.data.data.bpmStatus
                    });
                }
               
            }
        })
    },
    // 获取经纬度
    gotGps: function () {
        var that = this;
        wx.getLocation({
            type: 'wgs84',
            success(res) {
                that.setData({
                    latitude: res.latitude,
                    longitude: res.longitude
                });

            }
        });
    },
    openGps: function () {
        var that = this;
        wx.chooseLocation({
            success: function (res) {
                that.setData({
                    latitude: res.latitude,
                    longitude: res.longitude
                })
            }
        })
    },
    //拍照上传
    goPhotoUpload: function () {
        wx.navigateTo({
            url: "/pages/photoUpload/photoUpload?orderNo=" + this.data.orderNo
        })
    },
    //录像上传
    goVideoUpload: function () {
        wx.navigateTo({
            url: "/pages/videoUpload/videoUpload?orderNo=" + this.data.orderNo
        })
    },
    // 地图
    goGps: function () {
        console.log('latitude=' + this.data.latitude)
        wx.navigateTo({
            url: "/pages/gps/gps?latitude=" + this.data.latitude + '&longitude=' + this.data.longitude + '&proName=' + this.data.proName
        })
    },
    onReady: function () {

    },

    //保存
    saveBtn: function (e) {
        var that = this;
        var data = {
            id: that.data.id,
            posJingdu: that.data.latitude,
            posWeidu: that.data.longitude,
            updateBy: that.data.openId,
            posRemark: that.data.posRemark
        };
        console.log("data:" + JSON.stringify(data));

        wx.request({
            url: pathUrl + '/rest/posDetailController/' + that.data.id,
            data: data,
            method: 'PUT',
            success: function (res) {
                console.log(res.data)
                if (res.data.ok) {
                    that.setData({
                        isSubmit: true
                    });
                    wx.showToast({
                        title: '保存成功',
                        duration: 500,
                        mask: true,
                        success: function () {
                            setTimeout(function () {
                                wx.switchTab({
                                    url: "/pages/indexb/indexb"
                                })
                            }, 1000);
                        }
                    })
                } else {
                    that.setData({
                        isSubmit: true
                    });
                    wx.showToast({
                        title: '提交失败',
                        icon: 'none',
                        mask: true,
                        duration: 1000
                    })
                }
            }
        })

    },
    //提交
    submitBtn: function (e) {
        var that = this;
        var data = {
            id: that.data.id,
            bpmStatus: '已安装',
            updateBy: that.data.openId,
            posJingdu: that.data.latitude,
            posWeidu: that.data.longitude,
            posRemark: that.data.posRemark
        };
        console.log("data:" + JSON.stringify(data));


        wx.request({
            url: pathUrl + '/rest/posDetailController/' + that.data.id,
            data: data,
            method: 'PUT',
            success: function (res) {
                console.log(res.data)
                if (res.data.ok) {
                    that.setData({
                        isSubmit: true
                    });
                    wx.showToast({
                        title: '提交成功',
                        duration: 500,
                        mask: true,
                        success: function () {
                            setTimeout(function () {
                                wx.switchTab({
                                    url: "/pages/indexb/indexb"
                                })
                            }, 1000);
                        }
                    })
                } else {
                    that.setData({
                        isSubmit: true
                    });
                    wx.showToast({
                        title: '提交失败',
                        icon: 'none',
                        mask: true,
                        duration: 1000
                    })
                }
            }
        })

    },




    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})